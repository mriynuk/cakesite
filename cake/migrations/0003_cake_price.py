# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0002_auto_20150525_2323'),
    ]

    operations = [
        migrations.AddField(
            model_name='cake',
            name='price',
            field=models.PositiveIntegerField(default=0, max_length=5),
        ),
    ]
