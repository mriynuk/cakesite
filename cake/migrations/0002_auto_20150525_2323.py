# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=60)),
                ('description', models.TextField(null=True, blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('changed_date', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='cake',
            name='on_slider',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cake',
            name='category',
            field=models.ManyToManyField(related_name='category', to='cake.Category'),
        ),
    ]
