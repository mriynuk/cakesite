from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.cache import cache_page
from django.views.decorators.http import etag
from django.template import RequestContext
from cake.models import Category, Cake
# import sys


def etag_func_main(request):
    response = str(Cake.objects.filter(on_slider=True).latest("changed_date").changed_date)
    response += str(request.is_ajax())
    response += str(request.META.get("Content-Encoding"))
    return response


def etag_func_category(request, page_slug):
    response = str(Category.objects.all().latest("changed_date").changed_date)
    response += str(request.is_ajax())
    response += str(request.META.get("Content-Encoding"))
    return response


def etag_func_cake(request, cake_slug):
    response = str(Cake.objects.all().latest("changed_date").changed_date)
    response += str(request.is_ajax())
    response += str(request.META.get("Content-Encoding"))
    return response


@etag(etag_func_main)
@cache_page(60 * 10)
def main(request):
    """Main page."""
    cakes = Cake.objects.filter(on_slider=True).order_by("name")

    return render_to_response("slider.html", dict(cakes=cakes, active_page="home"), context_instance=RequestContext(request))


@cache_page(60 * 60)
def about_page(request):
    """About page."""
    return render_to_response("about.html", dict(active_page="about"), context_instance=RequestContext(request))


@cache_page(60 * 60)
def contact_page(request):
    """Contact page."""
    return render_to_response("contact.html", dict(active_page="contact"), context_instance=RequestContext(request))


@etag(etag_func_category)
@cache_page(60 * 10)
def category(request, page_slug):
    """Cakes related to category."""
    curent_category = get_object_or_404(Category, slug=page_slug)
    cakes = Cake.objects.filter(category=curent_category).order_by("name")

    return render_to_response("image-gallery.html", dict(cakes=cakes, active_page=page_slug), context_instance=RequestContext(request))


@etag(etag_func_cake)
@cache_page(60 * 5)
def cake(request, cake_slug):
    """Cake page."""
    cake = get_object_or_404(Cake, slug=cake_slug)

    return render_to_response("cake-page.html", dict(cake=cake), context_instance=RequestContext(request))
